#https://www.youtube.com/watch?v=a8V6b1iy-Hg

import csv

ACCEPTED_MSG = '''
Hi {},

We are thrilled to let you know that you have been accepted

{}

With best wishes

Mikey
'''

REJECTED_MSG = '''
Hi {},

Apologies, you have been rejected

With best wishes

Mikey
'''

with open('locations.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter = ',')
    next(csv_reader)

    for row in csv_reader:
        name, email, accepted, location = row
#        print(name, email, accepted, location)
        
        if accepted == "yes":
            msg = ACCEPTED_MSG.format(name, location)
        else:
            msg = REJECTED_MSG.format(name)
    
        print('Send email to: {}'.format(email))
        print('E-mail content:')
        print(msg)